<?php get_header(); ?>
    <div class="container">
		<?php if (have_posts()) :
			while (have_posts()) : the_post();
      if( function_exists('kama_breadcrumbs') ) kama_breadcrumbs();?>
      
        <h1><?php single_post_title(); ?> </h1>
        <?php

        if ( has_post_thumbnail()) : ?>
          <img src="<?php echo get_the_post_thumbnail_url(); ?>" />
        <?php endif;
				the_content();
			endwhile;
		endif; ?>
    </div>
<?php get_footer(); ?>
